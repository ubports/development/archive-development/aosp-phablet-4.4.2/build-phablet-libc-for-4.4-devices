Build phablet's libc for 4.4 devices
====================================

This repository hosts a .gitlab-ci.yml file used to build the libc.so from the Canonical's phablet-4.4.2_r1 codebase. The file will then replace the copy in the system.img provided from vendor(s).

Why this is needed?
-------------------

On some devices, the shipped Android system.img contains an incorrectly-patched libc.so. This makes [libhybris](https://github.com/libhybris/libhybris) unable to hook `__set_errno()` function. This bionic version of this function uses bionic's TLS system, conflicting with Glibc's implementation, resulting in crashes. So, we'll have to ship a correct libc.so in a system.img.

This repository lets GitLab CI build the correct libc.so for us. This creates a chain of custody, ensure that we shipped the binary built from the published source and not some backdoor'ed version.